import { Injectable } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Order } from './entities/order.entity';
import { OrderItem } from './entities/orderItem.entity';
import { User } from 'src/users/entities/user.entity';
import { Product } from 'src/products/entities/product.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order) private ordersRepository: Repository<Order>,
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Product) private productsRepository: Repository<Product>,
    @InjectRepository(OrderItem)
    private orderItemRepository: Repository<OrderItem>,
  ) {}
  async create(createOrderDto: CreateOrderDto) {
    const user = await this.usersRepository.findOneBy({
      id: createOrderDto.userId,
    });
    const order = new Order();
    order.user = user;
    order.total = 0;
    order.qty = 0;
    order.orderItem = [];
    for (const oi of createOrderDto.orderItem) {
      const orderItem = new OrderItem();
      orderItem.product = await this.productsRepository.findOneBy({
        id: oi.productId,
      });
      orderItem.name = orderItem.product.name;
      orderItem.price = orderItem.product.price;
      orderItem.qty = oi.qty;
      orderItem.total = orderItem.price * orderItem.qty;
      await this.orderItemRepository.save(orderItem);
      order.orderItem.push(orderItem);
      order.total += orderItem.total;
      order.qty += orderItem.qty;
    }
    return this.ordersRepository.save(order);
  }

  findAll() {
    return this.ordersRepository.find({ relations: { orderItem: true } });
  }

  findOne(id: number) {
    return this.ordersRepository.findOne({
      where: { id },
      relations: { orderItem: true },
    });
  }

  async update(id: number, updateOrderDto: UpdateOrderDto) {
    const updateOrder = await this.ordersRepository.findOne({
      where: { id },
    });
    await this.ordersRepository.update(id, {
      ...updateOrder,
      ...updateOrderDto,
    });
    const result = await this.ordersRepository.findOne({
      where: { id },
      relations: { orderItem: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteOrder = await this.ordersRepository.findOneOrFail({
      where: { id },
    });
    await this.ordersRepository.remove(deleteOrder);
    return deleteOrder;
  }
}
